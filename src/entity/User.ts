import {
    IsEmail,
    MinLength,
    IsDate
} from 'class-validator'
import {
    Column,
    CreateDateColumn,
    Entity,
    Index,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
    OneToMany
} from 'typeorm'
import { Exclude } from 'class-transformer'
import { Bearer } from './Bearer'

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    public id: number

    @Column({transformer: {
        to(value: string) {
            return value ? value.toLowerCase() : value
        },
        from(value: string) {
            return value
        }
    } })
    @IsEmail()
    @Index({unique: true})
    @MinLength(2)
    public email: string

    @Column({nullable: true})
    @Exclude()
    @Index({unique: true})
    public googleId: string

    @Column()
    public firstName: string

    @Column()
    public lastName: string

    @OneToMany(type => Bearer, bearer => bearer.user, { onDelete: 'CASCADE' })
    public bearers: Bearer[]

    @CreateDateColumn()
    @IsDate()
    public createdAt: Date

    @UpdateDateColumn()
    @IsDate()
    public updatedAt: Date

}
