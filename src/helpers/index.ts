import { validate } from 'class-validator'
import { getConnection, Connection } from 'typeorm'

export const env = {
    port: Number(process.env.PORT) || 3000,
    mode: process.env.NODE_ENV || 'production'
}

// Returns a database connection depending on NODE_ENV
export function getEnvConnection(): Connection {
    return getConnection( env.mode )
}

// Validates entity.
// If validation fails, an error of given type is thrown.
// Otherwise, nothing is returned
export async function mustValidate<T extends Error>(obj: any, err: {new(message: string): T; }) {
    const errors = await validate(obj)
    if ( errors.length > 0 ) {
        throw new err( errors.map( e => `Value '${e.value}' is invalid for property '${e.property}'` ).join( '\n' ) )
    }
}

export function parseAuth(scheme: string, auth: string) {
    if (auth === undefined)
        return undefined
    const trimmed = scheme.trim()
    if (trimmed.length === 0)
        return undefined
    const start = `${trimmed.substring(0, 1).toUpperCase()}${trimmed.substring(1).toLowerCase()} `
    if (auth.substring(0, start.length) !== start)
        return undefined
    return auth.substring(start.length) || undefined
}

export function parseBearer(auth: string): string {
    return parseAuth('Bearer', auth)
}

export function parseBasic(auth: string): string {
    return parseAuth('Basic', auth)
}
