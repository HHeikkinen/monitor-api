import * as moment from 'moment'

function getTimeStamp() {
    return moment().format( 'YY/MM/DD HH:mm:ss.SSS ZZ' )
}

// Wrapper for console.log
// This way logging behaviour is easily manipulated
export function log( ...args: any[] ) {
    // tslint:disable-next-line
    console.log( `[${getTimeStamp()}]`, ...args )
}

export function logImportant( title: any, ...args: any[] ) {
    log( `[:::${title.toString().toUpperCase()}:::]`, args )
}

export function logError( ...args: any[] ) {
    for ( let i = 0; i < args.length; i++ ) {
        const arg = args[i]
        const httpCode = Number( arg.httpCode )
        if ( !isNaN( httpCode ) ) {
            if ( httpCode >= 500 ) { // Print server-side errors clearly
                log( `==============] ERROR ${httpCode} [==============` )
            }
        }
        log(arg)
    }
    // else if ( !httpCode ) { // Http errors are already printed to console automatically
}
