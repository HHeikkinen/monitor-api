import * as express from 'express'
import { Server } from 'net'
import * as morgan from 'morgan'
import * as cors from 'cors'
import * as helmet from 'helmet'
import * as bodyParser from 'body-parser'
import { createConnection, getConnection } from 'typeorm'
import { Action, useExpressServer } from 'routing-controllers'
import { UserController } from './controller/UserController'
import { log, logError } from './helpers/log'
import { getEnvConnection, env, parseBearer } from './helpers'
import { Bearer } from './entity/Bearer'

export default class App {

    private app: express.Application
    private server: Server

    public async getApp(): Promise<any> {
        await this.init()
        return this.app
    }

    public async run(): Promise<any> {
        await this.init()

        // START the server
        this.server = this.app.listen(env.port, function () {
            log(`The server is running in port '${env.port}' in '${env.mode}' mode`)
        })
    }

    public async init(): Promise<express.Application> {

        /*
         *  Create our app w/ express
         */
        this.app = express()

        /*
         *  HELMET
         */
        this.app.use(helmet())

        /*
         *  CORS
         */
        this.app.use(cors())

        /*
         *  LOGGING
         */
        this.app.use(morgan('combined'))

        /*
         *  Body parsers and methods
         */

        this.app.use(bodyParser.urlencoded({
            extended: true,
            limit: '10kB',
        }))

        // Parse application/json
        this.app.use(bodyParser.json({
            limit: '10kB',
        }))

        /*
         *  Database
         */

        try {
            getConnection(env.mode)
        } catch (e) {
            await createConnection(env.mode)
                .catch(err => {
                    logError('Connection to database failed:', err)
                })
        }

        /*
         *  Setting routes
         */

        useExpressServer(this.app, {
            classTransformer: true,
            development: false,
            controllers: [
                UserController
            ],
            currentUserChecker: async (action: Action) => {
                try {
                    const bearer = parseBearer(action.request.headers.authorization)
                    const bearerEntity = await getEnvConnection()
                        .getRepository(Bearer)
                        .findOne({ where: { bearer } })
                    log('[AUTH]', bearer, bearerEntity)
                    return bearerEntity.user
                } catch (e) {
                    return null
                }
            },
        })

        return this.app
    }
}
