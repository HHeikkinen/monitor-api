import {
    Delete,
    Get,
    HeaderParam,
    JsonController,
    } from 'routing-controllers'
import { Repository } from 'typeorm'
import { Bearer } from '../entity/Bearer'
import { getEnvConnection } from '../helpers'

@JsonController('/bearers')
export class BearersController {

    private bearers: Repository<Bearer>

    constructor() {
        this.bearers = getEnvConnection().getRepository<Bearer>(Bearer)
    }

    /**
     *
     * @api {GET} /bearers Verify bearer
     * @apiName getBearer
     * @apiGroup bearers
     * @apiVersion 1.0.0
     *
     *
     * @apiHeader {String} bearer bearer
     *
     */

    @Get()
    public async verify(@HeaderParam('bearer') bearer: string) {
        return this.bearers.findOneOrFail({where: {bearer}})
    }

    /**
     *
     * @api {DELETE} /bearers Delete bearer
     * @apiName deleteBearer
     * @apiGroup bearers
     * @apiVersion 1.0.0
     *
     * @apiHeader {String} bearer bearer
     *
     */

    @Delete()
    public async delete(@HeaderParam('bearer') bearer: string) {
        return this.bearers.delete(bearer)
    }

}
