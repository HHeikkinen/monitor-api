import {
    BadRequestError,
    Body,
    CurrentUser,
    ForbiddenError,
    Get,
    JsonController,
    Param,
    Post,
    UnauthorizedError,
    InternalServerError,
    HttpError
} from 'routing-controllers'
import { Connection, Repository } from 'typeorm'
import { User } from '../entity/User'
import { logError } from '../helpers/log'
import { mustValidate, getEnvConnection } from '../helpers'
import * as uuid from 'uuid'
import { Bearer } from '../entity/Bearer'

@JsonController()
export class UserController {

    private users: Repository<User>
    private bearers: Repository<Bearer>
    private conn: Connection
    
    constructor() {
        this.conn = getEnvConnection()
        this.users = this.conn.getRepository(User)
        this.bearers = this.conn.getRepository(Bearer)
    }
    
    @Get('/users')
    public async all(@CurrentUser({required: true}) user: User) {
        return this.users.find({})
    }

}
