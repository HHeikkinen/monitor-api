import * as dotenv from 'dotenv'
dotenv.config()

import App from './app'

async function run() {
    await new App().run()
}

run()
